package es.cipfpbatoi.servicio;

import org.hibernate.Session;

public class FilmService {
	Session sesion;

	public FilmService() throws Exception {
		sesion = Conexion.getSession();
		if (sesion == null) {
			throw new Exception("Problemas al abrir la sesión de persistencia");
		}
	}

	public void cerrar() {
		Conexion.closeSession();
	}

	

}
